jQuery(function ($) {



    $('#login-validation').validate(
        {
            rules:
                {
                    username: { required: true, email: true, maxlength: 255},
                    password: { required: true }
                },
            messages:
                {
                    username: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters'},
                    password: { required: 'Please enter your password'}
                }
        }
    );


    $('#user-create-validation').validate(
        {
            rules:
                {
                    company: { required: true, maxlength: 225, remote:{url: "/admin/api/create/owner/checkCompany", type: "post"} },
                    firstName: { required: true, maxlength: 255 },
                    lastName: {required: true, maxlength: 255 },
                    phoneNumber: {required: true, digits: true},
                    email: { required: true, email: true, maxlength: 255, remote:{url: "/admin/api/create/owner/checkEmail", type: "post"}},
                    password: { required: true }
                },
            messages:
                {
                    company: { required: 'Please enter your Company Name', maxlength: 'Company Name cannot have more than 225 characters'},
                    firstName: { required: 'Please enter your first name', maxlength: 'Your first name cannot have more than 255 characters'},
                    lastName: { required: 'Please enter your last name', maxlength: 'Your last name cannot have more than 255 characters'},
                    phoneNumber: {required: 'Please enter your phone number', digits: 'Phone number can only contain digits'},
                    email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters', remote: "The email you provided already exists"},
                    password: { required: 'Please enter a password'}
                }
        }
    );



