package com.project.demo.mappers;

import com.project.demo.domain.User;
import com.project.demo.model.UserModel;
import org.springframework.stereotype.Component;


@Component
public class UserToUserModelMapper {

    public UserModel mapToUserModel(User user){
        UserModel model = new UserModel();
        model.setId(user.getId());
        model.setFirstName(user.getFirstName());
        model.setLastName(user.getLastName());
        model.setEmail(user.getEmail());
        model.setCompany(user.getCompany());
        return model;
    }








}
