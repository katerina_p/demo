package com.project.demo.mappers;

import com.project.demo.domain.User;
import com.project.demo.model.UserModel;

public class UserModelToUserMapper {

    public static User mapTo(UserModel userModel){
        User user = new User();
        user.setCompany(userModel.getCompany());
        user.setFirstName(userModel.getFirstName());
        user.setLastName(userModel.getLastName());
        user.setPhoneNumber(userModel.getPhoneNumber());
        user.setEmail(userModel.getEmail());
        user.setPassword(userModel.getPassword());


        return user;
    }
}
