package com.project.demo.domain;

import javax.persistence.*;


    @Entity
    @Table(name = "USER_TABLE")
    public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "user_id")
        private Long id;

        @Column(name = "first_name", nullable = false)
        private String firstName;

        @Column(name = "last_name", nullable = false)
        private String lastName;

        @Column(name = "phone_number", nullable = false, unique = true)
        private String phoneNumber;

        @Column(name = "email", nullable = false, unique = true)
        private String email;

        @Column(name = "password", nullable = false)
        private String password;

        @Column(name = "company", nullable = false)
        private String company;


        public User(String company, String firstName, String lastName, String phoneNumber, String email, String password) {

            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.password = password;
            this.company = company;
        }

        public User() {

        }

        public Long getId() { return id; }

        public void setId(Long id) { this.id = id; }

        public String getCompany() { return company; }

        public void setCompany(String company) { this.company = company; }

        public String getFirstName() { return firstName; }

        public void setFirstName(String firstName) { this.firstName = firstName; }

        public String getLastName() { return lastName; }

        public void setLastName(String lastName) { this.lastName = lastName; }

        public String getPhoneNumber() { return phoneNumber; }

        public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

        public String getEmail() { return email; }


        public void setEmail(String email) { this.email = email; }

        public String getPassword() { return password; }

        public void setPassword(String password) { this.password = password; }


        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("User{");
            sb.append("id=").append(id);
            sb.append(", company=").append(company);
            sb.append(", first name=").append(firstName);
            sb.append(", last name=").append(lastName);
            sb.append(", phone number=").append(phoneNumber);
            sb.append(", email=").append(email);
            sb.append(", password=").append(password);
            sb.append('}');
            return sb.toString();
        }


    }




