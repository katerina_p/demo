package com.project.demo.service;

import com.project.demo.domain.User;
import com.project.demo.model.UserModel;


public interface UserService {

    boolean checkCompany(String company);

    boolean checkCompany(String company, String id);

    public boolean checkEmail(String email);

    public boolean checkEmail(String email, String session);

    User createUser(UserModel userModel);

}
