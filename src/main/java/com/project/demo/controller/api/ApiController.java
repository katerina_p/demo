package com.project.demo.controller.api;


import com.project.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("user/api")
public class ApiController {

    @Autowired
    private UserService userService;



    @PostMapping(path="/create/user/checkCompany")
    public String CompanyExists1(@RequestParam("company") String company){
        return String.valueOf(!userService.checkCompany(company));
    }
    @PostMapping(path="/create/user/checkEmail")
    public String EmailExists(@RequestParam("email") String email){
        return String.valueOf(!userService.checkEmail(email));
    }

}
